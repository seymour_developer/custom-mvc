<?php
	
	namespace Controllers;
	
	use Helpers\Utils;
	use Models\Task;
	use Core\Controller;
	
	class IndexController extends Controller
	{
		public function index() {
			
			$model = new Task();
			$orderArray = [];
			
			if (isset($_GET['sort_username'])) {
				$orderArray['username'] = $_GET['sort_username'];
			}
			if (isset($_GET['sort_email'])) {
				$orderArray['email'] = $_GET['sort_email'];
			}
			if (isset($_GET['sort_status'])) {
				$orderArray['checked'] = $_GET['sort_status'];
			}
			
			if (!empty($orderArray)) {
				$model->computeOrder($orderArray);
			}
			
			$list = $model->all();
			
			$params['list'] = $list;
			$params['pageNumbers'] = $model->pageNumbers;
			$params['currentPage'] = $model->currentPage;
			
			if (!empty($_POST)) {
				$username = $_POST['username'];
				$email = $_POST['email'];
				$text = $_POST['text'];
				
				if (empty($username)) {
					$params['response']['errors'][] = Utils::t('task_add_err_empty_username');
				}
				if (empty($text)) {
					$params['response']['errors'][] = Utils::t('task_add_err_empty_text');
				}
				if (empty($email)) {
					$params['response']['errors'][] = Utils::t('task_add_err_empty_email');
				} else {
					if (!Utils::emailIsValid($email)) {
						$params['response']['errors'][] = Utils::t('task_add_err_email_invalid');
					}
				}
				
				if (empty($params['response']['errors'])) {
					$data = [
						'username' => $username,
						'email' => $email,
						'text' => $text
					];
					if ($model->add($data)) {
						$params['response'] = [
							'success' => true,
							'message' => Utils::t('task_added_successfully')
						];
						$list = $model->all();
						$params['list'] = $list;
					}
				}
			}
			
			return $this->render('index', $params);
		}
	}
