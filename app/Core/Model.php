<?php
	
	namespace Core;
	
	class Model
	{
		protected $db;
		protected $table;
		protected $limit;
		protected $totalRecords;
		protected $orderBy = "";
		protected $allowedOrderFields = ['username', 'checked', 'email'];
		public $pageNumbers;
		public $currentPage;
		
		public function __construct()
		{
			$this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
		}
		
		public function paginate($table, $limit=3) {
			$this->table = $table;
			$this->limit = $limit;
			$this->currentPage =  isset($_GET['page'])? $_GET['page'] : 1;
			$this->count();
			$this->getPaginationNumbers();
			return $this->getData();
		}
		
		public function computeOrder($data) {
			
			if (!empty($data) && is_array($data)) {
				
				foreach ($data as $key => $item) {
					if (!in_array($key, $this->allowedOrderFields)) {continue;}
					if (!in_array($item, ['asc', 'desc'])) {continue;}
					$this->orderBy .= ($key ." " . strtoupper($item) . ",");
				}
				
				if (!empty($this->orderBy)) {
					$this->orderBy = rtrim("ORDER BY $this->orderBy", ",");
				}
			}
		}
		
		public function count() {
			$stmt = $this->db->prepare("SELECT COUNT(id) FROM $this->table");
			$stmt->execute();
			$this->totalRecords = $stmt->fetchColumn();
		}
		
		public function getData() {
			$start = 0;
			if ($this->currentPage>1) {
				$start = ($this->currentPage * $this->limit) - $this->limit;
			}
			return $this->db->select("SELECT * FROM {$this->table} {$this->orderBy} LIMIT $start, $this->limit");
		}
		
		public function getPaginationNumbers() {
			$this->pageNumbers = ceil($this->totalRecords / $this->limit);
		}
	}
