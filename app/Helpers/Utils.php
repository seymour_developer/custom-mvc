<?php
	
	namespace Helpers;
	
	class Utils
	{
		
		public static function emailIsValid($email) {
			
			if (empty($email)) {return false;}
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return false;
			}
			
			return true;
		}
		
		public static function t($key) {
			$file = include BASE_PATH.'/app/Translates/ru.php';
			return $file[$key];
		}
		
		public static function url($url = '', $params = []) {
			$str = BASE_URL;
			
			if (!empty($url)) {
				$str .= $url;
			}
			
			$str .= '?'.http_build_query(array_merge($_GET, $params));
			
			return $str;
		}
	}
