$(document).ready(function () {
	
	let baseUrl = window.location.origin;
	
	$('.task-checked').on('change', function() {
		let task = $(this).data('id');
		$.ajax({
			method: 'GET',
			contentType:"application/json; charset=utf-8",
			dataType:"json",
			url:  baseUrl + "/task/check?id=" + task,
			success: function(data){
				console.log(data);
			}
		});
	});
	
	$('.sorting').on('click', function () {
		let value = $(this).data('sort'),
			field = $(this).data('field');
		let url = new URL(location.href);
		url.searchParams.set("sort_"+field, value);
		location.href = url.href;
	});
});