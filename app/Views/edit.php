<?php
    if (!empty($params['response']['success'])) { ?>
        <div class="alert alert-success" role="alert">
            <?=$params['response']['message']?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
<?php }
	
	if (!empty($params['response']['errors'])) {
		foreach ($params['response']['errors'] as $error) { ?>
            <div class="alert alert-danger" role="alert">
				<?=$error;?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
		<?php }
	}
?>

<form method="post" action="">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="username">Имя пользователя</label>
				<input type="text" disabled class="form-control" value="<?=$params['task']['username']?>" />
			</div>
			
			<div class="form-group email-form">
				<label for="email">Email</label>
				<input type="email" class="form-control" disabled value="<?=$params['task']['email']?>" />
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="text">Текст задачи</label>
				<textarea name="text" id="text" cols="5" rows="5" class="form-control"><?=$params['task']['text']?></textarea>
				<small id="text" class="form-text text-muted"></small>
			</div>
		</div>
	</div>
    <div class="d-flex justify-content-center">
        <button type="submit" class="btn btn-primary">Редактировать</button>
    </div>
</form>
