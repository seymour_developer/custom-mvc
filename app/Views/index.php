
    <?php
		if (!empty($params['response']['errors'])) {
			foreach ($params['response']['errors'] as $error) { ?>
				<div class="alert alert-danger" role="alert">
					<?=$error;?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
				</div>
			<?php }
		}
		
		if (!empty($params['response']['success'])) { ?>
            <div class="alert alert-success" role="alert">
				<?=$params['response']['message']?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php }
	
	    use Helpers\Utils; ?>

    <!-- Task form -->
	<form method="post" action="">
        <div class="row">
	        <div class="col-md-6">
		        <div class="form-group">
			        <label for="username">Имя пользователя</label>
			        <input type="text" name="username" class="form-control" id="username" />
			        <small id="emailHelp" class="form-text text-muted"></small>
		        </div>
	        
		        <div class="form-group email-form">
			        <label for="email">Email</label>
			        <input type="email" name="email" class="form-control" id="email" />
			        <small id="email" class="form-text text-muted"></small>
		        </div>
	        </div>
			<div class="col-md-6">
		        <div class="form-group">
		            <label for="text">Текст задачи</label>
		            <textarea name="text" id="text" cols="5" rows="5" class="form-control"></textarea>
		            <small id="text" class="form-text text-muted"></small>
		        </div>
			</div>
        </div>
        <div class="d-flex justify-content-center">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
	</form>

    <!-- Tasks list -->
    <table class="table">
		<thead>
	        <tr>
	            <th scope="col">#</th>
	            <th scope="col">Имя пользователя  <i class="fa fa-sort sorting" data-field="username" data-sort="<?=isset($_GET['sort_username']) && $_GET['sort_username']=='asc' ? 'desc' : 'asc';?>" aria-hidden="true"></i></th>
	            <th scope="col">Email  <i class="fa fa-sort sorting" data-field="email" data-sort="<?=isset($_GET['sort_email']) && $_GET['sort_email']=='asc' ? 'desc' : 'asc'?>" aria-hidden="true"></i></th>
	            <th scope="col">Текст задачи</th>
				<?php if (empty($_SESSION['user'])) { ?>
                    <th scope="col">Статус  <i class="fa fa-sort sorting" data-field="status" data-sort="<?=isset($_GET['sort_status']) && $_GET['sort_status']=='asc' ? 'desc' : 'asc'?>" aria-hidden="true"></i></th>
                <?php } ?>
	            <th scope="col"></th>
                <?php if (!empty($_SESSION['user'])) { ?>
                    <th scope="col"></th>
                    <th scope="col">Статус</th>
                <?php } ?>
	        </tr>
		</thead>
		<tbody>
			<?php foreach ($params['list'] as $task) { ?>
		        <tr>
		            <th scope="row"><?=$task['id']?></th>
		            <td><?=htmlspecialchars($task['username'])?></td>
		            <td><?=$task['email']?></td>
		            <td><?=htmlspecialchars($task['text'])?></td>
					<?php if (empty($_SESSION['user'])) { ?>
		                <td><?=$task['checked']? 'Выполнено'  : 'Не выполнено'?></td>
                    <?php } ?>
		            <td><?=$task['modified']? 'Отредактировано администратором'  : ''?></td>
					<?php if (!empty($_SESSION['user'])) { ?>
                        <td>
                            <a href="<?=Utils::url('task/edit', ['id' => $task['id']])?>" class="btn btn-primary">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </th>
                        <td>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input data-id="<?=$task['id']?>" class="form-check-input task-checked" type="checkbox" value="1" <?=$task['checked']? 'checked' : '';?>>
                                    Выполнено
                                </label>
                            </div>
                        </td>
					<?php } ?>
		        </tr>
			<?php } ?>
		</tbody>
	</table>

    <!-- Pagination -->
    <?php if ($params['pageNumbers']>0 && ($params['currentPage']>=1 && $params['currentPage']<=$params['pageNumbers'])) { ?>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="<?=Utils::url($params['currentPage']>1? '?page='.($params['currentPage']-1) : '#')?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <?php
	                if ($params['pageNumbers'] <= 5) {
	                    for ($counter = 1; $counter <= $params['pageNumbers']; $counter++){
			                if ($counter == $params['currentPage']) {
				                echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
			                } else {
				                echo "<li class='page-item'><a class='page-link' href='" . Utils::url('', ['page' => $counter]) . "'>$counter</a></li>";
			                }
		                }
	                } else {
	                    
	                    if ($params['currentPage']>=5) {
                            echo "<li class='page-item'><a class='page-link' href='" . Utils::url('', ['page' => 1]) . "'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link'>...</a></li>";
                        }
                        
                        for (
                            $counter = $params['currentPage']>=5? ($params['currentPage'] - 2) : 1;
                            $counter <= ($params['pageNumbers'] - ($params['currentPage']+2) >= 2? $params['currentPage'] + 2 : $params['pageNumbers']);
                            $counter++
                        ) {
                            if ($counter == $params['currentPage']) {
                                echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                            } else {
                                echo "<li class='page-item'><a class='page-link' href='" . Utils::url('', ['page' => $counter]) . "'>$counter</a></li>";
                            }
                        }
                        
                        if ($params['pageNumbers'] - ($params['currentPage'] + 2) >= 2) {
                            echo "<li class='page-item'><a class='page-link'>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' href='" . Utils::url('', ['page' => $params['pageNumbers']]) . "'>{$params['pageNumbers']}</a></li>";
                        }
	                }
                ?>
                <li class="page-item">
                    <a class="page-link" href="<?=Utils::url('', $params['currentPage']<$params['pageNumbers']? ['page' => $params['currentPage']+1] : []);?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    <?php } ?>
