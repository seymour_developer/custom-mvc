<?php
	
	ob_start();
	session_start();
	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	require_once 'config.php';
	
	spl_autoload_register(function ($className) {
		$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
		include_once $_SERVER['DOCUMENT_ROOT'] . '/app/' . $className . '.php';
	});
	
	$bootstrap = new \Core\Bootstrap();
	$bootstrap->init();
